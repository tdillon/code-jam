export default async function () {
  const root = "./src/2008/0-qualification-round/saving-the-universe";

  for await (const dirEntry of Deno.readdir(root)) {
    if (!dirEntry.name.endsWith(".in")) {
      continue;
    }

    console.log(`Parsing ${dirEntry.name}`);

    const outputFile = await Deno.create(`${root}/${dirEntry.name}.out`);
    const encoder = new TextEncoder();

    const decoder = new TextDecoder("utf-8");
    const data = await Deno.readFile(`${root}/${dirEntry.name}`);
    const lines = decoder.decode(data).split("\n");

    let i = 0;
    const N = +lines[i++];

    for (let c = 1; c <= N; ++c) {
      let numSearchEngines = +lines[i++];
      let searchEngines = [];
      for (let s = 0; s < numSearchEngines; ++s) {
        searchEngines.push(lines[i++]);
      }

      let numQueries = +lines[i++];
      let queries = [];
      let queryIndexes = [];
      for (let q = 0; q < numQueries; ++q) {
        queries.push(lines[i++]);
        queryIndexes.push(searchEngines.indexOf(lines[i - 1]));
      }

      console.log(numSearchEngines, numQueries, queryIndexes);

      let numSwitches = 0;
      let a = [...Array(numSearchEngines)].map(() => false);

      queryIndexes.forEach((i) => {
        a[i] = true;

        if (a.every((v) => v)) { // true when no available name
          a = [...Array(numSearchEngines)].map(() => false);
          ++numSwitches;
        }

        a[i] = true;
      });

      let solution = `Case #${c}: ${numSwitches}\n`;
      console.log(solution);

      await Deno.write(outputFile.rid, encoder.encode(solution));
    }
  }
}
