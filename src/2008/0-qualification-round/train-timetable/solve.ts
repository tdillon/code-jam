export default async function () {
  const root = "./src/2008/0-qualification-round/train-timetable";

  for await (const dirEntry of Deno.readdir(root)) {
    if (!dirEntry.name.endsWith(".in")) {
      continue;
    }

    console.log(`Parsing ${dirEntry.name}`);

    const outputFile = await Deno.create(`${root}/${dirEntry.name}.out`);
    const encoder = new TextEncoder();

    const decoder = new TextDecoder("utf-8");
    const data = await Deno.readFile(`${root}/${dirEntry.name}`);
    const lines = decoder.decode(data).split("\n");

    let i = 0;
    const N = +lines[i++];

    for (let c = 1; c <= N; ++c) {
      let T = +lines[i++]; // Turnaround Time
      let [NA, NB] = lines[i++].split(" ").map((s) => +s);

      let trips = [];
      for (let t = 0; t < NA + NB; ++t) {
        let [departure, arrivalString] = lines[i++].split(" ");
        let arrival = new Time(arrivalString, T).value;
        trips.push({ station: t < NA ? "A" : "B", departure, arrival });
      }
      trips = trips.sort((trip1, trip2) =>
        trip1.departure.localeCompare(trip2.departure)
      );

      console.log(`N:${N}`, `c:${c}`, `T:${T}`, `NA:${NA}`, `NB:${NB}`, trips);

      let numTrainsStartA = 0, numTrainsStartB = 0, currentA = 0, currentB = 0;

      for (let hour = 0; hour < 24; ++hour) {
        for (let min = 0; min < 60; ++min) {
          let time = `${`0${hour}`.substring(
            hour.toString().length - 1,
          )}:${`0${min}`.substring(min.toString().length - 1)}`;

          trips.filter((a) => a.arrival === time).forEach((a) => {
            a.station === "A" ? ++currentB : ++currentA;
          });

          trips.filter((a) => a.departure === time).forEach((a) => {
            a.station === "A" ? --currentA : --currentB;
            if (currentA < 0) {
              ++numTrainsStartA;
              ++currentA;
            }
            if (currentB < 0) {
              ++numTrainsStartB;
              ++currentB;
            }
          });
        }
      }

      let solution = `Case #${c}: ${numTrainsStartA} ${numTrainsStartB}\n`;
      console.log(solution);

      await Deno.write(outputFile.rid, encoder.encode(solution));
    }
  }
}

class Time {
  private _time: string;

  constructor(private s: string, turnaroundTime: number) {
    let hour = +s.substring(0, 2);
    let min = +s.substring(3);

    min += turnaroundTime;

    if (min >= 60) {
      ++hour;
      min %= 60;
    }

    this._time = hour < 24
      ? `${`0${hour}`.substring(hour.toString().length - 1)}:${`0${min}`
        .substring(min.toString().length - 1)}`
      : "99:99";
  }

  public get value(): string {
    return this._time;
  }
}
