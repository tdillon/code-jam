import a from "./2008/0-qualification-round/saving-the-universe/solve.ts";
import b from "./2008/0-qualification-round/train-timetable/solve.ts";

// TODO It would be nice to use dynamic imports.

let solvers = new Map([
  [ "saving-the-universe", a ],
  [ "train-timetable", b ]
])

let solve = solvers.get(Deno.args[0]);

if (solve) {
    await solve();
} else {
    let keys = [];
    for (let k of solvers.keys()) {
        keys.push(`  - ${k}`);
    }
    console.error(`Acceptable problem names:\n${keys.join('\n')}`);
}
