# Code Jam

My solutions for Google's Code Jam contests.  https://codingcompetitions.withgoogle.com/codejam

## Generic Usage Instructions

All `*.in` files in the problems directory will be evaluated.
A corresponding `*.out` file will be created.

To delete all `.out` files

```bash
find src -name *.out | xargs rm
```

## Example Usage

```bash
# outputs available <problem-name>
deno src/runner.ts

# run the solver for a given problem
deno --allow-write --allow-read src/runner.ts <problem-name>
```
